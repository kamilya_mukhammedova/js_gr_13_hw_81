import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Link } from '../../model/link.model';
import { environment } from '../../environments/environment';

@Injectable({
  providedIn: 'root'
})
export class LinksService {

  constructor(private http: HttpClient) { }

  addLink(link: Link) {
    const newLink = {
      originalUrl: link.originalUrl
    };
     return this.http.post(environment.apiUrl + '/links', newLink);
  }
}
