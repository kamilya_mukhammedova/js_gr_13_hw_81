import { Component, ViewChild } from '@angular/core';
import { NgForm } from '@angular/forms';
import { LinksService } from './service/links.service';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.sass']
})

export class AppComponent {
  @ViewChild('f') form!: NgForm;
  shortenUrl = '';

  constructor(private linksService: LinksService) {}

  onSubmit() {
    this.linksService.addLink(this.form.value).subscribe((url: {[key: string]: any}) => {
      this.shortenUrl = url['shortenUrl'];
    });
  }
}
