const express = require('express');
const {customAlphabet} = require('nanoid');
const Link = require('../model/Link');
const router = express.Router();

router.post('/', async (req, res, next) => {
  try {
    if(req.body.originalUrl === '') {
      return res.status(400).send({message: 'Url is required!'});
    }
    const shortenNanoid = customAlphabet('1234567890abcdef', 6);
    const linkData = {
      originalUrl: req.body.originalUrl,
      shortenUrl: shortenNanoid(),
    };
    const link = new Link(linkData);
    await link.save();
    return res.send({shortenUrl: link.shortenUrl});
  } catch (e) {
    next(e);
  }
});

router.get('/:shortenUrl', async (req, res, next) => {
  try {
    const shortenLink = await Link.findOne({shortenUrl: req.params.shortenUrl});
    if(shortenLink === null) {
      return res.status(404).send({message: 'Not found'});
    } else {
      res.status(301).redirect(shortenLink.originalUrl);
    }
  } catch (e) {
    next(e);
  }
});

module.exports = router;